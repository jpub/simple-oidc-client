$(document).ready(async function () {
    const urlParams = new URLSearchParams(window.location.search);
    const grantCode = urlParams.get('grantCode');
    const idToken = urlParams.get('idToken');
    
    $('#auth-uri').val($.cookie('auth-uri'));
    $('#redirect-uri').val($.cookie('redirect-uri'));
    $('#client-id').val($.cookie('client-id'));
    $('#token-endpoint').val($.cookie('token-endpoint'));
    $('#client-secret').val($.cookie('client-secret'));
    $('#idp-uri').html($.cookie('idp-uri'));    
    $('#grant-code').html(grantCode);
    $('#logout-uri').val($.cookie('logout-uri'));
    if (idToken) {
        $.cookie('idToken', idToken);
        const decoded = jwt_decode(idToken);
        $('#id-token').html(JSON.stringify(decoded, null, 4));
    }       
});

function login(service, callback) {    
    const authUri = $('#auth-uri').val();
    const redirectUri = $('#redirect-uri').val();
    const clientId = $('#client-id').val();
    const scope = $('#scope').val();
    const state = $('#state').val();
    const nonce = $('#nonce').val();
    const tokenEndpoint = $('#token-endpoint').val();
    const clientSecret = $('#client-secret').val();
    const logoutUri = $('#logout-uri').val();
    const idpUri = `${authUri}?client_id=${clientId}&redirect_uri=${redirectUri}&scope=${scope}&response_type=code&response_mode=query&state=${state}&nonce=${nonce}`;
    if (authUri && redirectUri && clientId && scope && state && nonce && tokenEndpoint && clientSecret) {
        $.cookie('auth-uri', authUri);
        $.cookie('redirect-uri', redirectUri);
        $.cookie('client-id', clientId);
        $.cookie('idp-uri', idpUri);
        $.cookie('token-endpoint', tokenEndpoint);
        $.cookie('client-secret', clientSecret);
        $.cookie('logout-uri', logoutUri);
        $('#idp-uri').html(idpUri);

        const login_data = Qs.stringify({
            'authUri': authUri,
            'rediretUri': redirectUri,
            'clientId': clientId,
            'scope': scope,
            'state': state,
            'nonce': nonce,
            'clientSecret': clientSecret,
            'tokenEndpoint': tokenEndpoint,
        });        
        window.location.href = `/login?${login_data}`;        
    }        
}

function clearCookies() {    
    var cookies = $.cookie();
    for(var cookie in cookies) {
       $.removeCookie(cookie);
    }
    window.location.href = '/';
}

function logout() {
    const logoutUrl = $.cookie('logout-uri');
    const idToken = $.cookie('idToken');
    window.location.href = `${logoutUrl}?id_token_hint=${idToken}&post_logout_redirect_uri=${window.location.protocol}//${window.location.host}`;
}