import express from 'express';
import axios from 'axios';
import url from 'url';
import qs from 'qs';

export const app = express();

app.set('views', 'views');
app.use(express.static('public'));

const PORT = process.argv[2] || 9000;

var tokenEndpoint = '';
var clientId = '';
var clientSecret = '';
var redirectUri = '';

const buildUrl = (base: string, options: Map<string, string>): string => {
    const newUrl = new url.URL(base);
    options.forEach((value, key) => {
        newUrl.searchParams.append(key, value);
    });
    return newUrl.href;
};

app.get('/login', function(req: express.Request, res: express.Response): void {
    const authUri = req.query.authUri as string;
    redirectUri = req.query.rediretUri as string;
    clientId = req.query.clientId as string;
    const scope = req.query.scope as string;
    const state = req.query.state as string;
    const nonce = req.query.nonce as string;
    clientSecret = req.query.clientSecret as string;    
    tokenEndpoint = req.query.tokenEndpoint as string;        

    const queryParms = new Map<string, string>();
    queryParms.set('client_id', clientId);
    queryParms.set('redirect_uri', redirectUri);
    queryParms.set('scope', scope);
    queryParms.set('response_type', 'code');
    queryParms.set('response_mode', 'query');
    queryParms.set('state', state);
    queryParms.set('nonce', nonce);
    const idpUri = buildUrl(authUri, queryParms);    
    res.redirect(idpUri);
});

app.get('/callback', async function(req: express.Request, res: express.Response): Promise<void> {
    const state = req.query.state as string;
    const grantCode = req.query.code as string;

    if (!grantCode) {
        console.log(req.query);
        res.status(500).redirect('/?error=no-grant-code');
        return;
    }

    const form_data = qs.stringify({
        'grant_type': 'authorization_code',
        'code': grantCode,
        'client_id': clientId,
        'client_secret': clientSecret,
        'redirect_uri': redirectUri
    });

    // not doing any sanity on state and nonce
    const idpResponse = await axios.post(tokenEndpoint, form_data, {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        }
    });
    console.log(idpResponse.data);
    res.status(200).redirect(`/?grantCode=${grantCode}&idToken=${idpResponse.data.id_token}`);
});

if (PORT == '9001') {
    console.log('simple client: http://devlocal-frontendauth.gpls.pro:9001')
} else if (PORT == '9002') {
    console.log('simple client: http://devlocal-frontendweb.gpls.pro:9002')
} else {
    console.log(`simple client: http://localhost:${PORT}`)
}
export const server = app.listen(PORT);
